# com.codeberg.fydron.calculator

### A basic C++ calculator app

This repository is for my personal calculator app and his evolution as my skills in C++ grows.

Right now some improvements are made: </br>
    [x] CMake support added

Some other important changes are wating to be made:
    [ ] Unit Testing support
    [ ] Code refactoring
    [ ] CI/CD build pipeline