// Bibliotecas
#include <iostream>

// Função principal
int main(int argc, char const *argv[])
{
    // Variáveis
    char operadores;
    float numero1, numero2;

    // Escolha de operadores
    std::cout << "Escolha um operador (+, -, *, /): ";
    std::cin >> operadores;

    // Entrada de operandos
    std::cout << "Digite dois operandos: ";
    std::cin >> numero1 >> numero2;

    // Resultado das operações
    switch (operadores)
    {
    case '+':
        std::cout << numero1 << " + " << numero2 << " = " << numero1 + numero2;
        break;

    case '-':
        std::cout << numero1 << " - " << numero2 << " = " << numero2 - numero2;
        break;

    case '*':
        std::cout << numero1 << " * " << numero2 << " = " << numero1 * numero2;
        break;

    case '/':
        std::cout << numero1 << " / " << numero2 << " = " << numero1 / numero2;

    default:
        // Se o operador for diferente de: +, -, * ou /, uma mensagem de erro será apresentada:
        std::cout << "Erro! Operador não identificado!";
        break;
    }

    return 0;
}
